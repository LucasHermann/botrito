use anyhow::{Context, Result};
use futures_util::StreamExt;
use irc::client::prelude::*;
use tokio::sync::mpsc;
use tracing::{debug, info, warn};

use train::Bot;

struct IrcActor<B: Bot> {
    receiver: mpsc::Receiver<String>,
    transmitter: mpsc::Sender<String>,
    bot: B,
    config_path: String,
}

impl<B: Bot> IrcActor<B> {
    fn new(
        receiver: mpsc::Receiver<String>,
        transmitter: mpsc::Sender<String>,
        bot: B,
        config_path: String,
    ) -> Self {
        Self {
            receiver,
            transmitter,
            bot,
            config_path,
        }
    }
}

async fn run_irc_actor<B: Bot>(mut actor: IrcActor<B>) {
    // let mut client = Client::from_config(config).await?;
    let mut client = Client::new(&actor.config_path).await.unwrap();
    client.identify().unwrap();
    let mut stream = client.stream().unwrap();
    info!("Connected to IRC server as {}", client.current_nickname());

    let mut chan = "".to_string();
    loop {
        tokio::select! {
            opt_msg = stream.next() => {
                match opt_msg.transpose().unwrap() {
                    None => break,
                    Some(message) => {
                        if let Command::PRIVMSG( ref channel, ref msg) = message.command {
                            if msg.starts_with(client.current_nickname()) {
                                info!("New message addressed to bot");
                                debug!("{}", msg);
                                if let Some(sender) = message.source_nickname() {
                                    if actor.bot.handle_message(sender.to_string(), msg.to_string(), actor.transmitter.clone()).is_ok() {
                                        info!("Message succesfully handled. Message: {:?}", msg);
                                        let _ = client.send_privmsg(channel, format!("{}: Ok", sender));
                                        chan = channel.clone();
                                    } else {
                                        warn!("Failed to process message {}", msg);
                                        let _ = client.send_privmsg(channel, "Something went wrong try again ヽ(。_°)ノ");
                                    };
                                };
                            }
                        }
                    }
                };
            },
            Some(msg) = actor.receiver.recv() => {
                client.send_privmsg(&chan, &msg).unwrap();
            },
        }
    }
}

pub fn connect_irc<B>(config_path: String, bot: B) -> tokio::task::JoinHandle<()>
where
    B: Bot + std::marker::Send + 'static,
{
    let (transmitter, receiver) = mpsc::channel(16);
    let actor = IrcActor::new(receiver, transmitter, bot, config_path);
    let handle = tokio::spawn(run_irc_actor(actor));
    info!("Connected to IRC");
    handle
}
