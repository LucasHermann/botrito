use time::OffsetDateTime;
use tokio::sync::{mpsc, oneshot};
use tracing::{debug, info};

use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::sync::{Arc, Mutex};

use super::train;

#[derive(Default, Clone)]
pub struct Timetable {
    iner: Arc<Mutex<HashMap<String, mpsc::Sender<TrainCommand>>>>,
}

impl Timetable {
    pub fn schedule_train<S>(&self, train: S, departure: OffsetDateTime) -> Option<train::Train>
    where
        S: Into<String>,
    {
        let mut lock = self.iner.lock().unwrap();
        let train = train.into();
        if let Entry::Vacant(e) = lock.entry(train.clone()) {
            debug!("Creating new Train {} departing at {}", train, departure);
            let (train_transmitter, train_receiver) = mpsc::channel(16);
            let train = train::Train::new(train_receiver, &train, departure);
            e.insert(train_transmitter);
            info!("Train created");
            Some(train)
        } else {
            None
        }
    }

    pub fn remove<S>(&self, train: S) -> Option<mpsc::Sender<TrainCommand>>
    where
        S: Into<String>,
    {
        let mut lock = self.iner.lock().unwrap();
        let train = train.into();
        lock.remove(&train)
    }

    pub fn get_train_transmitter<S>(&self, train: S) -> Option<mpsc::Sender<TrainCommand>>
    where
        S: Into<String>,
    {
        let lock = self.iner.lock().unwrap();
        let train = train.into();
        lock.get(&train).map(|transmitter| transmitter.to_owned())
    }
}

#[derive(Debug)]
pub enum TrainCommand {
    AttendeeJoin {
        respond_to: oneshot::Sender<String>,
        attendee: String,
    },
    AttendeeLeave {
        respond_to: oneshot::Sender<String>,
        attendee: String,
    },
    ChangeDeparture {
        respond_to: oneshot::Sender<String>,
        departure: OffsetDateTime,
    },
}

impl std::fmt::Display for TrainCommand {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            TrainCommand::AttendeeJoin {
                respond_to: _,
                attendee,
            } => {
                write!(
                    f,
                    "TrainCommand::AttendeeJoin - attendee name: {}",
                    attendee
                )
            }
            TrainCommand::AttendeeLeave {
                respond_to: _,
                attendee,
            } => {
                write!(
                    f,
                    "TrainCommand::AttendeeLeave - attendee name: {}",
                    attendee
                )
            }
            TrainCommand::ChangeDeparture {
                respond_to: _,
                departure,
            } => {
                write!(
                    f,
                    "TrainCommand::ChangeDeparture - new departure: {}",
                    departure
                )
            }
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum ControllerCommand {
    Create {
        train: String,
        departure: OffsetDateTime,
    },
    Join(String),
    Leave(String),
}
