use anyhow::{bail, Result};
use time::macros::time;
use time::{Duration, OffsetDateTime};
use tokio::sync::mpsc;
use tokio::time::sleep;
use tracing::{debug, info, warn};

use std::collections::HashSet;

use super::types;

#[derive(Debug)]
pub struct Train {
    receiver: mpsc::Receiver<types::TrainCommand>,
    name: String,
    departure: OffsetDateTime,
    attendees: HashSet<String>,
}

impl Train {
    pub fn new<S>(
        receiver: mpsc::Receiver<types::TrainCommand>,
        name: S,
        departure: OffsetDateTime,
    ) -> Self
    where
        S: Into<String>,
    {
        Train {
            receiver,
            departure,
            name: name.into(),
            attendees: HashSet::<String>::new(),
        }
    }

    fn get_name(&self) -> String {
        self.name.clone()
    }

    fn join<A>(&mut self, attendee: A)
    where
        A: Into<String>,
    {
        let attendee = attendee.into();
        debug!("Adding {} to attendees", attendee);
        self.attendees.insert(attendee);
    }

    fn leave<A>(&mut self, attendee: A)
    where
        A: Into<String>,
    {
        let attendee = attendee.into();
        debug!("Removing {} to attendees", attendee);
        self.attendees.remove(&attendee);
    }

    fn depart(&self) -> String {
        format!(
            "{}, is now departing! {}",
            self.name,
            self.attendees
                .iter()
                .map(|a| a.as_str())
                .collect::<Vec<&str>>()
                .join(", ")
        )
    }

    fn handle_command(&mut self, command: types::TrainCommand) {
        match command {
            types::TrainCommand::AttendeeJoin {
                respond_to,
                attendee,
            } => {
                self.join(attendee);
                let _ = respond_to.send("welcome on board!".to_string());
            }
            types::TrainCommand::AttendeeLeave {
                respond_to,
                attendee,
            } => {
                self.leave(attendee);
                let _ = respond_to.send("sorry to see you leave".to_string());
            }
            types::TrainCommand::ChangeDeparture {
                respond_to,
                departure,
            } => {
                self.departure = departure;
                let attendees = self
                    .attendees
                    .iter()
                    .map(|a| a.as_str())
                    .collect::<Vec<&str>>()
                    .join(", ");
                let _ =
                    respond_to.send(format!("{} {} has changed departure", attendees, self.name));
            }
        }
    }
}

pub async fn run_train(
    mut train: Train,
    timetable: types::Timetable,
    transmitter: mpsc::Sender<String>,
) -> Result<()> {
    loop {
        let now = OffsetDateTime::now_utc();
        if train.departure < now {
            warn!("Can't run Train with departure set in the past.");
            bail!("Can't run Train with departure set in the past.");
        } else if train.departure.date() != now.date() {
            info!(
                "{} is not departing today sleeping until tomorrow",
                train.name
            );
            let tomorrow = now
                .saturating_add(Duration::days(1))
                .replace_time(time!(0:00));
            let delay = std::time::Duration::from_secs_f32((tomorrow - now).as_seconds_f32());
            sleep(delay).await;
        } else {
            info!("Starting Train {}", train.name);
            let delay =
                std::time::Duration::from_secs_f32((train.departure - now).as_seconds_f32());
            info!("Train will depart in {:?} seconds", delay);
            tokio::select! {
                Some(command) = train.receiver.recv() => {
                    train.handle_command(command);
                },
                _ = sleep(delay) => {
                    let name = train.get_name();
                    let _ = transmitter.send(train.depart()).await;
                    if timetable.remove(&name).is_none() {
                        warn!("Failed to delette train: {:?} from the timetable after departure", &name);
                    }
                    break
                },
                else => break,
            }
        }
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    use tokio::sync::oneshot;
    use tokio::time::sleep;

    #[test]
    fn create_train() {
        let (_, receiver) = mpsc::channel(8);
        let name = "Test name";
        let now = OffsetDateTime::now_utc();

        let result = Train::new(receiver, name, now);

        assert_eq!(
            result.name, name,
            "Invalid Train name. Excpected {:?} got {:?}",
            name, result.name
        );
        assert_eq!(
            result.departure, now,
            "Invalid Train departure. Excpected {:?} got {:?}",
            now, result.departure
        );
        assert_eq!(result.attendees, HashSet::<String>::new());
    }

    #[test]
    fn join_train() {
        let (_, receiver) = mpsc::channel(8);
        let name = "Test name";
        let now = OffsetDateTime::now_utc();
        let attendee = "foo";
        let expected_length = 1;

        let mut train = Train::new(receiver, name, now);

        train.join("foo");
        let result = &train.attendees;

        assert_eq!(
            result.len(),
            expected_length,
            "Invalid quantity of attendees. Expected {:?} got {:?}",
            expected_length,
            result.len()
        );
        assert!(
            result.contains(attendee),
            "Invalid attendee name. Expect {:?} got {:?}",
            attendee,
            result.iter().next()
        );
    }

    #[test]
    fn leave_train() {
        let (_, receiver) = mpsc::channel(8);
        let name = "Test name";
        let now = OffsetDateTime::now_utc();
        let attendees = ["foo", "bar", "baz"];
        let expected_length = 2;

        let mut train = Train::new(receiver, name, now);

        attendees.iter().for_each(|&attendee| train.join(attendee));

        train.leave("bar");
        let result = &train.attendees;

        assert_eq!(
            result.len(),
            expected_length,
            "Invalid quantity of attendees. Expected {:?} got {:?}",
            expected_length,
            result.len()
        );
        assert!(
            result.contains("foo"),
            "Invalid attendee name. Expect {:?} got {:?}",
            "foo",
            result.iter().next()
        );
        assert!(
            result.contains("baz"),
            "Invalid attendee name. Expect {:?} got {:?}",
            "baz",
            result.iter().next()
        );
    }

    #[tokio::test]
    async fn run_train_no_change() {
        let (mpsc_sender, mut mpsc_receiver) = mpsc::channel(8);
        let name = "Test name";
        let start = OffsetDateTime::now_utc();
        let departure = start.saturating_add(Duration::seconds(5));
        let timetable: types::Timetable = Default::default();

        let expected_msg = String::from(format!("{}, is now departing! ", name));

        let train = timetable.schedule_train(name, departure).unwrap();

        tokio::spawn(run_train(train, timetable, mpsc_sender));

        loop {
            tokio::select! {
                Some(msg) = mpsc_receiver.recv() => {
                    assert_eq!(msg, expected_msg, "Invalid departure message. Expected {:?} got {:?}", expected_msg, msg);
                    let delay = OffsetDateTime::now_utc() - start;
                    assert!(delay.whole_seconds() == 5, "Delay before departure is not as expected. Delay: {:?}", delay);
                    break
                },
                _ = sleep(std::time::Duration::from_secs_f32(Duration::seconds(6).as_seconds_f32())) => {
                    assert!(false, "time out reached");
                    break
                }
            }
        }
    }

    #[tokio::test]
    async fn run_train_change_departure() {
        let (transmitter, mut receiver) = mpsc::channel(8);
        let name = "Test name";
        let start = OffsetDateTime::now_utc();
        let departure = start.saturating_add(Duration::seconds(5));
        let timetable: types::Timetable = Default::default();

        let expected_msg = String::from(format!("{}, is now departing! ", name));

        let train = timetable.schedule_train(name, departure).unwrap();

        let (respond_to, oneshot_recv) = oneshot::channel();
        tokio::spawn(run_train(train, timetable.clone(), transmitter));
        let _ = timetable
            .get_train_transmitter(name)
            .unwrap()
            .send(types::TrainCommand::ChangeDeparture {
                respond_to,
                departure: start.saturating_add(Duration::seconds(10)),
            })
            .await;
        oneshot_recv.await.expect("Train task has been killed");

        loop {
            tokio::select! {
                Some(msg) = receiver.recv() => {
                    assert_eq!(msg, expected_msg, "Invalid departure message. Expected {:?} got {:?}", expected_msg, msg);
                    let delay = OffsetDateTime::now_utc() - start;
                    assert!(delay.whole_seconds() == 10, "Delay before departure is not as expected. Delay: {:?}", delay);
                    break
                },
                _ = sleep(std::time::Duration::from_secs_f32(Duration::seconds(11).as_seconds_f32())) => {
                    assert!(false, "time out reached");
                    break
                }
            }
        }
    }

    #[tokio::test]
    async fn run_train_with_past_departure() {
        let (transmitter, _) = mpsc::channel(8);
        let name = "Test name";
        let start = OffsetDateTime::now_utc();
        let departure = start - Duration::seconds(5);
        let timetable: types::Timetable = Default::default();

        let train = timetable.schedule_train(name, departure).unwrap();

        let res = tokio::spawn(run_train(train, timetable, transmitter))
            .await
            .unwrap();

        assert!(
            res.is_err(),
            "Running Train with departure set in the past did not return an Err. Got: {:?}",
            res
        );
    }
}
